var cheerio = require('cheerio');

exports.links = (elem, selector, baseUrl) => {
  var $ = cheerio.load(elem);
  
  return $(selector)
    .map((i, link) => $(link).attr('href'))
    .get()
    .map(x => baseUrl + x);
}

exports.text = (elem, selector) => {
  var $ = cheerio.load(elem);

  return $(selector)
    .text();
}

return module.exports;