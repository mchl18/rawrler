exports.clean = (text) => {
  return text
          .replace(' Telefonnummer anzeigen', '')
          .split('  ')
          .map((part) => {
            if (part.includes('@')){
              return part.split("").reverse().join("")
            }
            return part;
          })
          .join(', ');
}

return module.exports;