exports.url = {
  base: 'https://www.wlw.de',
  site: '/de/firmen/versandhandelsabwicklung-fulfillment?q=Versandhandelsabwicklung+%28Fulfillment%29&filter%5Bsort%5D=package&q=Versandhandelsabwicklung+(Fulfillment)&utf8=%E2%9C%93&filter%5Bcountry%5D%5B%5D=de&filter%5Bregion%5D=&filter%5Bradius%5D=50&filter%5Blat%5D=&filter%5Blng%5D=&utf8=%E2%9C%93&filter%5Bsupplier_types%5D%5B%5D=service_provider'
};

exports.fields = [
  '.company-name',
  '.company-address',
  '.contact-details',
];

return module.exports;