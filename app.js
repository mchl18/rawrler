/**
 * Simple throwaway tool for crawling
 */

var express = require('express');
var app     = express();
var config = require('./config');
var _ = require('lodash');

var requestHelpers = require('./request');
var cheerioHelpers = require('./cheerio-lib');
var utils = require('./utils');

var bodyParser = require('body-parser');

app.use(bodyParser.raw({
  type: '*/*'
}));

app.get('/links', async  (req, res) => {
    await requestHelpers
      .getLinks('a.btn-primary')
      .then(links => res.send(_.uniq(links)))
      .catch(e => res.status(400).send(e));
});

app.get('/pages', async (req, res) => {
  const link = req.params['link'];

  await requestHelpers
    .getPage(link)
    .then(page => res.send(page))
    .catch(e => res.status(400).send(e))
});

getTextFromField = (page) => {
  config.fields.map(field => cheerioHelpers.text(page, field))
}

app.post('/details', async (req, res) => {
  const links = req.body.toString();
  let results = [];

  if (!links) {
    res.send('no link')
  }

  // grab links from body as raw
  const json = JSON.parse(links.toString());
  const jsonLinks = json.links;

  // grab all pages for given links
  const pages = await Promise.all(
    jsonLinks.map(async (link) => {
      return await requestHelpers
        .getPage(link)
        .catch(e => res.status(400).send(e))
    })
  )
  .catch(e => res.send(e));

  // take all selectors from config
  results = config.fields
    .map((field) =>{
      return pages.map((page) => {
        // return cleaned text val for each
        const text = cheerioHelpers.text(page, field);
        return utils.clean(text);
      })}
    );

  // Yeah, I know.. but it works!
  res.status(200).send(results[0].map((result, i) => { 
    return {name: results[0][i], address: results[1][i], details: results[2][i]} 
  }))
});

app.listen('8081')
console.log('Omnomnomnom happens on port 8081');
exports = module.exports = app;