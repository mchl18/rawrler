var request = require('request');
var config = require('./config');
var cheerioHelpers = require('./cheerio-lib');

exports.getLinks = (selector) => new Promise((resolve, reject) => {

  request(config.url.base + config.url.site, (error, response, html) => {

    if(!error){
      // grab all links with specified selector
      resolve(cheerioHelpers.links(html, selector, config.url.base));
    } else {
      reject('error');
    }
  })
});

exports.getPage = (link) => new Promise((resolve) => {
  request(link, (error, response, html) =>  {
    if (error) {
      resolve(error, response)
      return
    }
    // console.log(html)
    resolve(html)
  });
});

